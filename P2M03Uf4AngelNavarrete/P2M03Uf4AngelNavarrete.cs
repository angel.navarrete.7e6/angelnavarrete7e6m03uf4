﻿using System;

namespace P2M03Uf4AngelNavarrete
{
    class P2M03Uf4AngelNavarrete
    {
        abstract class FiguraGeometrica
        {
            //Parametres
            protected int codi { get; set; }
            protected string nom { get; set; }
            protected ConsoleColor color { get; set; }

            //Constructors
            public FiguraGeometrica() { }
            public FiguraGeometrica(int codi, string nom, ConsoleColor color)
            {
                this.codi = codi;
                this.nom = nom;
                this.color = color;
            }
            public FiguraGeometrica(FiguraGeometrica figura)
            {
                this.codi = figura.codi;
                this.nom = figura.nom;
                this.color = figura.color;
            }
            //Metodes
            override
            public string ToString()
            {
                return "Codi " + codi + ", nom " + nom + ", Color " + color;
            }
            public abstract double Area();

        }

        class Rectangle : FiguraGeometrica
        {
            //Parametres
            private double baseRectangle { get; set; }
            private double altura { get; set; }

            //Constructors
            public Rectangle() { }

            public Rectangle(int codi, string nom, ConsoleColor color, double baseRectangle, double altura)
            {
                this.codi = codi;
                this.nom = nom;
                this.color = color;
                this.baseRectangle = baseRectangle;
                this.altura = altura;
            }
            public Rectangle(Rectangle figura)
            {
                this.codi = figura.codi;
                this.nom = figura.nom;
                this.color = figura.color;
                this.baseRectangle = figura.baseRectangle;
                this.altura = figura.altura;
            }

            //Metodes
            override
            public string ToString()
            {
                return base.ToString() +", base " + baseRectangle + ", altura " + altura;

            }
            public double Perimetre()
            {
                return (2 * baseRectangle) + (2 * altura);
            }

            public override double Area()
            {
                return baseRectangle * altura;
            }
            public override bool Equals(object obj)
            {
                if ((obj == null) || GetType() != obj.GetType())
                {
                    return false;
                }
                Rectangle p = (Rectangle)obj;
                if (this.codi == p.codi)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        class Triangle : FiguraGeometrica
        {
            //Parametres
            private double baseTriangle { get; set; }
            private double altura { get; set; }

            //Constructors
            public Triangle() { }

            public Triangle(int codi, string nom, ConsoleColor color, double baseTriangle, double altura)
            {
                this.codi = codi;
                this.nom = nom;
                this.color = color;
                this.baseTriangle = baseTriangle;
                this.altura = altura;
            }
            public Triangle(Triangle figura)
            {
                this.codi = figura.codi;
                this.nom = figura.nom;
                this.color = figura.color;
                this.baseTriangle = figura.baseTriangle;
                this.altura = figura.altura;
            }

            //Metodes
            override
            public string ToString()
            {
                return base.ToString() + ", base " + baseTriangle + ", altura " + altura;
            }
            public double Perimetre()
            {
                return baseTriangle * 3;
            }
            public override double Area()
            {
                return (baseTriangle * altura) / 2;
            }
            public override bool Equals(object obj)
            {
                if ((obj == null) || GetType() != obj.GetType())
                {
                    return false;
                }
                Triangle p = (Triangle)obj;
                return (this.codi == p.codi);

            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }
        class Cercle : FiguraGeometrica 
        {
            //Parametres
            private double radi { get; set; }

            //Constructors
            public Cercle() { }

            public Cercle(int codi, string nom, ConsoleColor color, double radi)
            {
                this.codi = codi;
                this.nom = nom;
                this.color = color;
                this.radi = radi;
            }
            public Cercle(Cercle figura)
            {
                this.codi = figura.codi;
                this.nom = figura.nom;
                this.color = figura.color;
                this.radi = figura.radi;
            }

            //Metodes
            override
            public string ToString()
            {
                return base.ToString() + ",radi " + radi;
            }
            public double Perimetre()
            {
                return 2 * Math.PI * radi;
            }
            public override double Area()
            {
                return Math.PI * Math.Pow(radi, 2);
            }
            public override bool Equals(object obj)
            {
                if ((obj == null) || GetType() != obj.GetType())
                {
                    return false;
                }
                Cercle p = (Cercle)obj;
                return (this.codi == p.codi);

            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }
        class ProvesFigures
        {
            static void Main(string[] args)
            {
                //PROVES CLASSE RECTANGLE
                Console.WriteLine("CLASE RECTANGLE");
                Rectangle rectangle1 = new Rectangle();
                Console.WriteLine("RECTANGLE 1: "+rectangle1.ToString());
                Rectangle rectangle2 = new Rectangle(2, "Rectasaurio", ConsoleColor.Red, 21, 43);
                Console.WriteLine("RECTANGLE 2: " +rectangle2.ToString());
                Console.WriteLine("Perimetre " + rectangle2.Perimetre());
                Console.WriteLine("Area " + rectangle2.Area());
                Rectangle rectangle3 = new Rectangle();
                Console.WriteLine("RECTANGLE 3: " + rectangle3.ToString());
                Console.WriteLine("-EQUALS:");
                Console.WriteLine("Es el rectangulo 1 igual al 2?: " + rectangle1.Equals(rectangle2));
                Console.WriteLine("Es el rectangulo 2 igual al 3?: " + rectangle2.Equals(rectangle3));
                Console.WriteLine("Es el rectangulo 1 igual al 3?: " + rectangle1.Equals(rectangle3));
                Console.WriteLine("-HASHCODE:");
                Console.WriteLine("HASCODE RECTANGULO 1: "+rectangle1.GetHashCode());
                Console.WriteLine("HASCODE RECTANGULO 2: "+rectangle2.GetHashCode());
                Console.WriteLine("HASCODE RECTANGULO 3: "+rectangle3.GetHashCode() + "\n\n");

                //PROVES CLASSE TRIANGLE
                Console.WriteLine("CLASE TRIANGLE");
                //triangle1.nom = "Triangularis";
                Triangle triangle1 = new Triangle(4, "Obamid", ConsoleColor.Black, 22, 69);
                Console.WriteLine("TRIANGLE 1: "+triangle1.ToString());
                Console.WriteLine("Perimetre " + triangle1.Perimetre());
                Console.WriteLine("Area " + triangle1.Area());
                Triangle triangle2 = new Triangle(4, "Pentagono", ConsoleColor.Red, 30, 90);
                Console.WriteLine("TRIANGLE 2: " + triangle2.ToString());
                Triangle triangle3 = new Triangle();
                Console.WriteLine("TRIANGLE 3: " + triangle3.ToString());
                //Console.WriteLine(triangle2.GetHashCode());
                Console.WriteLine("-EQUALS:");
                Console.WriteLine("Es el triangulo 1 igual al 2?: " + triangle1.Equals(triangle2));
                Console.WriteLine("Es el triangulo 2 igual al 3?: " + triangle2.Equals(triangle3));
                Console.WriteLine("Es el triangulo 1 igual al 3?: " + triangle1.Equals(triangle3));
                Console.WriteLine("-HASHCODE:");
                Console.WriteLine("HASCODE TRIANGULO 1: " + triangle1.GetHashCode());
                Console.WriteLine("HASCODE TRIANGULO 2: " + triangle2.GetHashCode());
                Console.WriteLine("HASCODE TRIANGULO 3: " + triangle3.GetHashCode() + "\n\n");

                //PROVES CLASE CERCLE
                Console.WriteLine("CLASE CERCLE");
                Cercle cercle1 = new Cercle();
                Console.WriteLine(cercle1.ToString());
                Cercle cercle2 = new Cercle(8, "ELCIRCULO", ConsoleColor.Blue, 88);
                Console.WriteLine(cercle2.ToString());
                Console.WriteLine("Area " + cercle2.Area());
                Console.WriteLine("Perimetre " + cercle2.Perimetre());
                Cercle cercle3 = new Cercle(7, "ELNOCIRCULO", ConsoleColor.Cyan, 13);
                Console.WriteLine(cercle3.ToString());
                Console.WriteLine("Area " + cercle3.Area());
                Console.WriteLine("Perimetre " + cercle3.Perimetre());
                Console.WriteLine("-EQUALS:");
                Console.WriteLine("Es el Circulo 1 igual al 2?: " + cercle1.Equals(cercle2));
                Console.WriteLine("Es el Circulo 2 igual al 3?: " + cercle2.Equals(cercle3));
                Console.WriteLine("Es el Circulo 1 igual al 3?: " + cercle1.Equals(cercle3));
                Console.WriteLine("-HASHCODE:");
                Console.WriteLine("HASCODE CIRCULO 1: " + cercle1.GetHashCode());
                Console.WriteLine("HASCODE CIRCULO 2: " + cercle2.GetHashCode());
                Console.WriteLine("HASCODE CIRCULO 3: " + cercle3.GetHashCode() + "\n\n");
            }
        }
    }
}
