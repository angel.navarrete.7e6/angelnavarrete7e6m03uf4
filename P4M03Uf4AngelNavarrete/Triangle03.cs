﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P4M03Uf4AngelNavarrete
{
    class Triangle : FiguraGeometrica
    {
        //Parametres
        private double BaseTriangle { get; set; }
        private double Altura { get; set; }

        //Constructors
        public Triangle() { }

        public Triangle(int codi, string nom, ConsoleColor color, double baseTriangle, double altura)
        {
            this.Codi = codi;
            this.Nom = nom;
            this.Color = color;
            this.BaseTriangle = baseTriangle;
            this.Altura = altura;
        }
        public Triangle(Triangle figura)
        {
            this.Codi = figura.Codi;
            this.Nom = figura.Nom;
            this.Color = figura.Color;
            this.BaseTriangle = figura.BaseTriangle;
            this.Altura = figura.Altura;
        }

        //Metodes
        override
        public string ToString()
        {
            return "Codi " + Codi + ", nom " + Nom + ", Color " + Color + ", base " + BaseTriangle + ", altura " + Altura;
        }
        public double Perimetre()
        {
            return BaseTriangle * 3;
        }
        public override double Area()
        {
            return (BaseTriangle * Altura) / 2;
        }
        public override bool Equals(object obj)
        {
            if ((obj == null) || GetType() != obj.GetType())
            {
                return false;
            }
            Triangle p = (Triangle)obj;
            return (this.Codi == p.Codi);

        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
