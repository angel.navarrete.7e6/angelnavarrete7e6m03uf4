﻿using System;
using System.Collections.Generic;

namespace P4M03Uf4AngelNavarrete
{
    class ProvaOrdenar
    {
        static void Main()
        {
            //PROVES CLASSE RECTANGLE
            Console.WriteLine("CLASE RECTANGLE");
            Rectangle rectangle1 = new Rectangle();
            Console.WriteLine("RECTANGLE 1: " + rectangle1.ToString());
            Rectangle rectangle2 = new Rectangle(2, "Rectasaurio", ConsoleColor.Red, 21, 43);
            Console.WriteLine("RECTANGLE 2: " + rectangle2.ToString());
            Console.WriteLine("Area " + rectangle2.Area());
            Rectangle rectangle3 = new Rectangle();
            Console.WriteLine("RECTANGLE 3: " + rectangle3.ToString());
            Console.WriteLine();


            //PROVES CLASSE TRIANGLE
            Console.WriteLine("CLASE TRIANGLE");
            Triangle triangle1 = new Triangle(4, "Obamid", ConsoleColor.Black, 22, 69);
            Console.WriteLine("TRIANGLE 1: " + triangle1.ToString());
            Console.WriteLine("Area " + triangle1.Area());
            Triangle triangle2 = new Triangle(4, "Pentagono", ConsoleColor.Red, 30, 90);
            Console.WriteLine("TRIANGLE 2: " + triangle2.ToString());
            Triangle triangle3 = new Triangle();
            Console.WriteLine("TRIANGLE 3: " + triangle3.ToString());

            //PROVES CLASE CERCLE
            Console.WriteLine("CLASE CERCLE");
            Cercle cercle1 = new Cercle();
            Console.WriteLine(cercle1.ToString());
            Cercle cercle2 = new Cercle(8, "ELCIRCULO", ConsoleColor.Blue, 88);
            Console.WriteLine(cercle2.ToString());
            Console.WriteLine("Area " + cercle2.Area());
            Cercle cercle3 = new Cercle(7, "ELNOCIRCULO", ConsoleColor.Cyan, 13);
            Console.WriteLine(cercle3.ToString());
            Console.WriteLine("Area " + cercle3.Area());


            //CREACIO DE DATA 1
            Console.WriteLine("-DATA 1: ");
            Data data1 = new Data(4, 6, 2002);
            Console.WriteLine(data1.ToString());

            Console.WriteLine("\n-DATA 2: ");
            //CREACIO DE DATA 2
            Data data2 = new Data(29, 7, 2023);
            Console.WriteLine(data2.ToString());

            //CREACIO DE DATA PER DEFECTE I DESPRES AMB SET ASIGNAR LA DATA
            Console.WriteLine("\n-DATA 3: ");
            Data data3 = new Data();
            data3.Dia = 10;
            data3.Mes = 7;
            data3.Any = 2002;
            Console.WriteLine(data3.ToString());
            //CREEM LLISTA DE FIGURES PER A LA COMPARACIO
            IOrdenable[] listaFiguras = new IOrdenable[9];
            listaFiguras[0] = triangle1;
            listaFiguras[1] = triangle2;
            listaFiguras[2] = triangle3;
            listaFiguras[3] = rectangle1;
            listaFiguras[4] = rectangle2;
            listaFiguras[5] = rectangle3;
            listaFiguras[6] = cercle1;
            listaFiguras[7] = cercle2;
            listaFiguras[8] = cercle3;
            Console.WriteLine("\nLISTA FIGURAS GEOMETRICAS ABANS DEL ORDENAR");
            foreach (FiguraGeometrica item in listaFiguras)
            {
                Console.WriteLine(item.ToString() + ",Area: " + item.Area());
            }
            FiguraGeometrica.Ordenar(listaFiguras);
            Console.WriteLine("\nLISTA FIGURAS GEOMETRICAS DESPRES DEL ORDENAR");
            foreach (FiguraGeometrica item in listaFiguras)
            {
                Console.WriteLine(item.ToString() + ",Area: " + item.Area());
            }

            //CREEM LLISTA DE DATA PER A LA COMPARACIO
            Console.WriteLine("\nLISTA DATA ABANS DEL ORDENAR");
            IOrdenable[] lista = new IOrdenable[3];
            lista[0] = data1;
            lista[1] = data2;
            lista[2] = data3;
            foreach (var item in lista)
            {
                Console.WriteLine(item);
            }

            Data.Ordenar(lista);
            Console.WriteLine("\nLISTA DATA DESPRES DEL ORDENAR");
            foreach (var item in lista)
            {
                Console.WriteLine(item);
            }

        }
    }
}
