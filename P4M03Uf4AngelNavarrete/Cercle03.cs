﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P4M03Uf4AngelNavarrete
{
    class Cercle : FiguraGeometrica, IOrdenable
    {
        //Parametres
        private double Radi { get; set; }

        //Constructors
        public Cercle() { }

        public Cercle(int codi, string nom, ConsoleColor color, double radi)
        {
            this.Codi = codi;
            this.Nom = nom;
            this.Color = color;
            this.Radi = radi;
        }
        public Cercle(Cercle figura)
        {
            this.Codi = figura.Codi;
            this.Nom = figura.Nom;
            this.Color = figura.Color;
            this.Radi = figura.Radi;
        }

        //Metodes
        override
        public string ToString()
        {
            return "Codi " + Codi + ", nom " + Nom + ", Color " + Color + ",radi " + Radi;
        }
        public double Perimetre()
        {
            return 2 * Math.PI * Radi;
        }
        public override double Area()
        {
            return Math.PI * Math.Pow(Radi, 2);
        }
        public override bool Equals(object obj)
        {
            if ((obj == null) || GetType() != obj.GetType())
            {
                return false;
            }
            Cercle p = (Cercle)obj;
            return (this.Codi == p.Codi);

        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
