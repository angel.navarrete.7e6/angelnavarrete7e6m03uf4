﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P4M03Uf4AngelNavarrete
{
    class Rectangle : FiguraGeometrica , IOrdenable
    {
        //Parametres
        private double BaseRectangle { get; set; }
        private double Altura { get; set; }

        //Constructors
        public Rectangle() { }

        public Rectangle(int codi, string nom, ConsoleColor color, double baseRectangle, double altura)
        {
            this.Codi = codi;
            this.Nom = nom;
            this.Color = color;
            this.BaseRectangle = baseRectangle;
            this.Altura = altura;
        }
        public Rectangle(Rectangle figura)
        {
            this.Codi = figura.Codi;
            this.Nom = figura.Nom;
            this.Color = figura.Color;
            this.BaseRectangle = figura.BaseRectangle;
            this.Altura = figura.Altura;
        }

        //Metodes
        override
        public string ToString()
        {
            return "Codi " + Codi + ", nom " + Nom + ", Color " + Color + ", base " + BaseRectangle + ", altura " + Altura;
        }
        public double Perimetre()
        {
            return (2 * BaseRectangle) + (2 * Altura);
        }

        public override double Area()
        {
            return BaseRectangle * Altura;
        }
        public override bool Equals(object obj)
        {
            if ((obj == null) || GetType() != obj.GetType())
            {
                return false;
            }
            Rectangle p = (Rectangle)obj;
            if (this.Codi == p.Codi)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
