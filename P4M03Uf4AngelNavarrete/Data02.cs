﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P4M03Uf4AngelNavarrete
{
    public interface IOrdenable
    {
        int Comparar(IOrdenable x);
        /* Retorna: 0 si this = x
        <0 si this < x
        >0 si this > x
        */
    }
    class Data : IOrdenable
    {
        //ATRIBUTS/GETTERS-SETTERS
        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Any { get; set; }

        private readonly int[] maxDiesPerMes = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        //CONSTRUCTORS
        public Data()
        {
            this.Dia = 1;
            this.Mes = 1;
            this.Any = 1980;
        }
        public Data(int dia, int mes, int any)
        {
            if (dia >= 1 && dia <= 31)
            {
                if (dia <= maxDiesPerMes[mes - 1])
                {
                    this.Dia = dia;
                }
            }
            else this.Dia = 1;
            if (mes >= 1 && mes <= 12) this.Mes = mes;
            else this.Mes = 1;
            if (dia >= 1) this.Any = any;
            else this.Any = 1980;
        }
        public Data(Data obj)
        {
            this.Dia = obj.Dia;
            this.Mes = obj.Mes;
            this.Any = obj.Any;
        }
        //METODES
        public void SumarDies(int dies)
        {
            if (dies >= 365)
            {
                this.Dia += (dies - 365);
                this.Any += 1;
            }
            else if (this.Dia + dies > maxDiesPerMes[this.Mes - 1])
            {
                this.Dia = (this.Dia + dies) - maxDiesPerMes[this.Mes - 1];
                this.Mes += 1;
            }
            else
            {
                this.Dia += dies;
            }
        }
        public void CalcularDiesDiferencia(Data data)
        {
            //Contadors
            int anysDiferencia;
            int contBisiest = 0;
            int messosDiferencia;
            int diesDiferencia;
            int diesTotals;
            //ANYS DE DIFERENCIA
            if (this.Any > data.Any) anysDiferencia = this.Any - data.Any;
            else if (this.Any == data.Any) anysDiferencia = 0;
            else
            {
                for (int i = this.Any; i < data.Any; i++)
                {
                    if (AnyBisiest(i) == true)
                    {
                        contBisiest++;
                    }
                }
                anysDiferencia = data.Any - this.Any;
            }
            //MESSOS DE DIFERENCIA
            if (this.Mes > data.Mes) messosDiferencia = this.Mes - data.Mes;
            else if (this.Mes == data.Mes) messosDiferencia = 0;
            else messosDiferencia = data.Mes - this.Mes;
            //DIES DIFERENCIA
            if (this.Dia > data.Dia) diesDiferencia = this.Dia - data.Dia;
            else if (this.Dia == data.Dia) diesDiferencia = 0;
            else diesDiferencia = data.Dia - this.Dia;
            //DIES TOTALS
            diesTotals = (anysDiferencia * 365) + (messosDiferencia * 31) + diesDiferencia + contBisiest;
            Console.WriteLine("Dies entre " + ToString() + " i " + data.ToString() + " es de " + diesTotals + " dies");
        }
        public bool AnyBisiest(int any)
        {
            if (any % 4 == 0 && any % 100 != 0 || any % 400 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public override bool Equals(object obj)
        {
            if ((obj == null) || GetType() != obj.GetType())
            {
                return false;
            }
            Data p = (Data)obj;
            if (this.Dia == p.Dia && this.Mes == p.Mes && this.Any == p.Any)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return this.Dia + "-" + this.Mes + "-" + this.Any;
        }

        public static void Ordenar(IOrdenable[] obj) {
            for (int x = 0; x < obj.Length; x++)
            {
                for (int y = 0; y < obj.Length - 1; y++)
                {
                    if (obj[y].Comparar(obj[y+1]) > obj[y+1].Comparar(obj[y]))
                    {
                        Data temporal = (Data)obj[y];
                        obj[y] = obj[y + 1];
                        obj[y + 1] = temporal;

                    }
                }
            }
        }
        public int Comparar(IOrdenable x)
        {
            Data date = (Data)x;
            if (this.Any == date.Any) return 0;
            else if (this.Any > date.Any) return 1;
            else return -1;
        }
    }
}
