﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P4M03Uf4AngelNavarrete
{
    abstract class FiguraGeometrica : IOrdenable
    {
        //Parametres
        protected int Codi { get; set; }
        protected string Nom { get; set; }
        protected ConsoleColor Color { get; set; }

        //Constructors
        public FiguraGeometrica() { }
        public FiguraGeometrica(int codi, string nom, ConsoleColor color)
        {
            this.Codi = codi;
            this.Nom = nom;
            this.Color = color;
        }
        public FiguraGeometrica(FiguraGeometrica figura)
        {
            this.Codi = figura.Codi;
            this.Nom = figura.Nom;
            this.Color = figura.Color;
        }
        //Metodes
        override
        public string ToString()
        {
            return "Codi " + Codi + ", nom " + Nom + ", Color " + Color;
        }
        public abstract double Area();

        public int Comparar(IOrdenable x)
        {
            FiguraGeometrica figura = (FiguraGeometrica)x;
            if (this.Area() == figura.Area()) return 0;
            else if (this.Area() <= figura.Area()) return 1;
            else return -1;
        }
        public static void Ordenar(IOrdenable[] obj)
        {
            for (int x = 0; x < obj.Length; x++)
            {
                for (int y = 0; y < obj.Length - 1; y++)
                {
                    if (obj[y].Comparar(obj[y + 1]) > obj[y + 1].Comparar(obj[y]))
                    {
                        FiguraGeometrica temporal = (FiguraGeometrica)obj[y];
                        obj[y] = obj[y + 1];
                        obj[y +1] = temporal;
                    }
                }
            }
        }
    }
}
