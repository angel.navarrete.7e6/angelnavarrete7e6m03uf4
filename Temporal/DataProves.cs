﻿using System;

namespace Temporal
{
    public class Data
    {
        //ATRIBUTS/GETTERS-SETTERS
        public int Dia { get; set; }
        public int Mes { get; set; }
        public int Any { get; set; }

        private readonly int[] maxDiesPerMes = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        //CONSTRUCTORS
        public Data()
        {
            this.Dia = 1;
            this.Mes = 1;
            this.Any = 1980;
        }
        public Data(int dia, int mes, int any)
        {
            if (dia >= 1 && dia <= 31)
            {
                if (dia<= maxDiesPerMes[mes-1])
                {
                    this.Dia = dia;
                }
            }
            else this.Dia = 1;
            if (mes >= 1 && mes <= 12) this.Mes = mes;
            else this.Mes = 1;
            if (dia >= 1) this.Any = any;
            else this.Any = 1980;
        }
        public Data(Data obj)
        {
            this.Dia = obj.Dia;
            this.Mes = obj.Mes;
            this.Any = obj.Any;
        }
        //METODES
        public void SumarDies (int dies)
        {
            if (dies >= 365)
            {
                this.Dia = this.Dia +(dies - 365);
                this.Any += 1;
            }
            else if(this.Dia+dies > maxDiesPerMes[this.Mes-1])
            {
                this.Dia = (this.Dia + dies)- maxDiesPerMes[this.Mes - 1];
                this.Mes += 1;
            }
            else
            {
                this.Dia += dies;
            }
        }
        public void CalcularDiesDiferencia(Data data) {
            //Contadors
            int anysDiferencia;
            int contBisiest = 0;
            int messosDiferencia;
            int diesDiferencia;
            int diesTotals;
            //ANYS DE DIFERENCIA
            if (this.Any > data.Any) anysDiferencia = this.Any - data.Any;
            else if (this.Any == data.Any) anysDiferencia = 0;
            else
            {
                for (int i = this.Any; i < data.Any; i++)
                {
                    if (AnyBisiest(i) == true)
                    {
                        contBisiest++;
                    }
                }
                anysDiferencia = data.Any - this.Any;
            }
            //MESSOS DE DIFERENCIA
            if (this.Mes > data.Mes) messosDiferencia = this.Mes - data.Mes;
            else if (this.Mes == data.Mes) messosDiferencia = 0;
            else messosDiferencia = data.Mes - this.Mes;
            //DIES DIFERENCIA
            if (this.Dia > data.Dia) diesDiferencia = this.Dia - data.Dia;
            else if (this.Dia == data.Dia) diesDiferencia = 0;
            else diesDiferencia = data.Dia - this.Dia;
            //DIES TOTALS
            diesTotals = (anysDiferencia * 365) + (messosDiferencia * 31) + diesDiferencia + contBisiest;
            Console.WriteLine("Dies entre "+ToString() +" i "+ data.ToString() + " es de "+diesTotals+ " dies");
        }
        public bool AnyBisiest(int any)
        {
            if (any % 4 == 0 && any % 100 != 0 || any % 400 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public override bool Equals(object obj)
        {
            if ((obj == null) || GetType() != obj.GetType())
            {
                return false;
            }
            Data p = (Data)obj;
            if (this.Dia == p.Dia && this.Mes == p.Mes && this.Any == p.Any)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return this.Dia + "-" + this.Mes + "-" + this.Any;
        }
    }
    class DataProves
    {
        static void Main(string[] args)
        {
            //CREACIO DE DATA 1
            Console.WriteLine("-DATA 1: ");
            Data data1 = new Data(4, 6, 2002);
            Console.WriteLine(data1.ToString());
            Console.WriteLine("Sumem 6 dies: ");
            data1.SumarDies(6);
            Console.WriteLine(data1.ToString());
            Console.WriteLine("Sumem 30 dies: ");
            data1.SumarDies(30);
            Console.WriteLine(data1.ToString());

            Console.WriteLine("\n-DATA 2: ");
            //CREACIO DE DATA 2
            Data data2 = new Data(29, 7, 2023);
            Console.WriteLine(data2.ToString());
            Console.WriteLine("Sumem 10 dies: ");
            data2.SumarDies(10);
            Console.WriteLine(data2.ToString());
            Console.WriteLine("Sumem 365 dies: ");
            data2.SumarDies(365);
            Console.WriteLine(data2.ToString());
            
            //CREACIO DE DATA PER DEFECTE I DESPRES AMB SET ASIGNAR LA DATA
            Console.WriteLine("\n-DATA 3: ");
            Data data3 = new Data();
            Console.WriteLine(data3.ToString());
            Console.WriteLine("Cambiem la data per defecte:");
            data3.Dia = 10;
            data3.Mes = 7;
            data3.Any = 2002;
            Console.WriteLine(data3.ToString());

            //COMPARACIONS EQUALS
            Console.WriteLine("\n- EQUALS:");
            Console.WriteLine(data1.ToString() + " es igual que "+data2.ToString() +"?: "+ data1.Equals(data2));
            Console.WriteLine(data1.ToString() + " es igual que " + data3.ToString() + "?: " + data1.Equals(data3));
            Console.WriteLine(data2.ToString() + " es igual que " + data3.ToString() + "?: " + data2.Equals(data3));

            //DIES DIFERENCIA
            Console.WriteLine("\n-METODE DIES DIFERENCIA:");
            data1.CalcularDiesDiferencia(data2);
            data1.CalcularDiesDiferencia(data3);
            Data data4 = new Data(29, 8, 2024);
            data2.CalcularDiesDiferencia(data4);
        }
    }
}
