﻿using System;

namespace PracticaInicial
{
    class FiguraGeometrica
    {
        //Parametres
        protected int codi { get; set; }
        protected string nom { get; set; }
        protected ConsoleColor color { get; set; }

        //Constructors
        public FiguraGeometrica() { }
        public FiguraGeometrica(int codi, string nom, ConsoleColor color)
        {
            this.codi = codi;
            this.nom = nom;
            this.color = color;
        }
        public FiguraGeometrica(FiguraGeometrica figura)
        {
            this.codi = figura.codi;
            this.nom = figura.nom;
            this.color = figura.color;
        }
        //Metodes
        override
        public string ToString()
        {
            return "Codi "+ codi + ", nom " + nom +", Color " + color;
        }
    }

    class Rectangle
    {
        //Parametres
        protected int codi { get; set; }
        protected string nom { get; set; }
        protected ConsoleColor color { get; set; }
        private double baseRectangle { get; set; }
        private double altura { get; set; }

        //Constructors
        public Rectangle() { }

        public Rectangle(int codi, string nom, ConsoleColor color, double baseRectangle, double altura)
        {
            this.codi = codi;
            this.nom = nom;
            this.color = color;
            this.baseRectangle = baseRectangle;
            this.altura = altura;
        }
        public Rectangle(Rectangle figura)
        {
            this.codi = figura.codi;
            this.nom = figura.nom;
            this.color = figura.color;
            this.baseRectangle = figura.baseRectangle;
            this.altura = figura.altura;
        }

        //Metodes
        override
        public string ToString()
        {
            return "Codi " + codi + ", nom " + nom + ", Color " + color + ", base " +baseRectangle + ", altura "+altura;
        }
        public double Perimetre()
        {
            return (2 * baseRectangle) + (2 * altura);
        }
        public double Area()
        {
            return baseRectangle * altura;
        }
    }

    class Triangle
    {
        //Parametres
        protected int codi { get; set; }
        protected string nom { get; set; }
        protected ConsoleColor color { get; set; }
        private double baseTriangle { get; set; }
        private double altura { get; set; }

        //Constructors
        public Triangle() { }

        public Triangle(int codi, string nom, ConsoleColor color, double baseTriangle, double altura)
        {
            this.codi = codi;
            this.nom = nom;
            this.color = color;
            this.baseTriangle = baseTriangle;
            this.altura = altura;
        }
        public Triangle(Triangle figura)
        {
            this.codi = figura.codi;
            this.nom = figura.nom;
            this.color = figura.color;
            this.baseTriangle = figura.baseTriangle;
            this.altura = figura.altura;
        }

        //Metodes
        override
        public string ToString()
        {
            return "Codi " + codi + ", nom " + nom + ", Color " + color + ", base " + baseTriangle + ", altura " + altura;
        }
        public double Perimetre()
        {
            return baseTriangle * 3;
        }
        public double Area()
        {
            return (baseTriangle * altura)/2;
        }
    }
    class Cercle
    {
        //Parametres
        protected int codi { get; set; }
        protected string nom { get; set; }
        protected ConsoleColor color { get; set; }
        private double radi { get; set; }

        //Constructors
        public Cercle() { }

        public Cercle(int codi, string nom, ConsoleColor color,double radi)
        {
            this.codi = codi;
            this.nom = nom;
            this.color = color;
            this.radi = radi;
        }
        public Cercle(Cercle figura)
        {
            this.codi = figura.codi;
            this.nom = figura.nom;
            this.color = figura.color;
            this.radi = figura.radi;
        }

        //Metodes
        override
        public string ToString()
        {
            return "Codi " + codi + ", nom " + nom + ", Color " + color+ ",radi " +radi;
        }
        public double Perimetre()
        {
            return 2 * Math.PI * radi;
        }
        public double Area()
        {
            return Math.PI * Math.Pow(radi, 2);
        }
    }
    class ProvesFigures
    {     
        static void Main(string[] args)
        {
            //PROVES CLASE FIGURA GEOMETRICA
            Console.WriteLine("CLASE FIGURA GEOMETRICA");
            FiguraGeometrica figura1 = new FiguraGeometrica();
            Console.WriteLine(figura1.ToString());
            FiguraGeometrica figura2 = new FiguraGeometrica(1,"Piramid",ConsoleColor.Red);
            Console.WriteLine(figura2.ToString());
            FiguraGeometrica figura3 = new FiguraGeometrica(figura2);
            Console.WriteLine(figura3.ToString());
            Console.WriteLine();

            //PROVES CLASSE RECTANGLE
            Console.WriteLine("CLASE RECTANGLE");
            Rectangle rectangle1 = new Rectangle();
            Console.WriteLine(rectangle1.ToString());
            Rectangle rectangle2 = new Rectangle(2,"Rectasaurio",ConsoleColor.Red,21, 43);
            Console.WriteLine(rectangle2.ToString());
            Console.WriteLine("Perimetre " + rectangle2.Perimetre());
            Console.WriteLine("Area "+rectangle2.Area());
            Console.WriteLine();

            //PROVES CLASSE TRIANGLE
            Console.WriteLine("CLASE TRIANGLE");
            Triangle triangle1 = new Triangle();
            Console.WriteLine(triangle1.ToString());

            Triangle triangle2 = new Triangle(4,"Obamid",ConsoleColor.Black, 22,69);
            Console.WriteLine(triangle2.ToString());
            Console.WriteLine("Perimetre "+triangle2.Perimetre());
            Console.WriteLine("Area " + triangle2.Area());
            Console.WriteLine();

            //PROVES CLASE CERCLE
            Console.WriteLine("CLASE CERCLE");
            Cercle cercle1 = new Cercle();
            Console.WriteLine(cercle1.ToString());
            Cercle cercle2 = new Cercle(8,"ELCIRCULO",ConsoleColor.Blue, 88);
            Console.WriteLine(cercle2.ToString());
            Console.WriteLine("Area " + cercle2.Area());
            Console.WriteLine("Perimetre "+cercle2.Perimetre());
            
        }
    }
}
